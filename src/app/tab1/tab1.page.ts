import { Component } from '@angular/core';
import {SearchService} from '../api/search.service';
import { LoadingController } from '@ionic/angular';
import { HistoryRecord } from '../models/history-record.model';
import { HistoryService } from '../api/history.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page 
{
  enteredInput:String = "";
  foodName:String;
  instructions:String;
  instructionTitle:String;
  ingredientsTitle: String;
  ingredientsArray:Array<string> = new Array(); 
  foodImage: String;
  loadingDialog: any;


  constructor(private searchService: SearchService, public loadingController: LoadingController, private historyService: HistoryService, private alertController: AlertController) 
  {
        
  }

  public btnClicked():void
  {
    if(this.enteredInput != "")
    {
      this.presentLoading();
      this.searchService.getMeal(this.enteredInput).subscribe( (data) => 
      {
        this.foodName = data['meals'][0]['strMeal'];
        this.instructionTitle = "Instructions: ";
        this.ingredientsTitle = "Ingredients: ";
        this.instructions = data['meals'][0]['strInstructions'];
        this.foodImage = data['meals'][0]['strMealThumb'];
        for(let i = 1; i <= 20; i++)
        {
          if(['strIngredient'+i] != null || ['strIngredient'+i].length >= 1)
          {
              //this.ingredients += data['meals'][0]['strMeasure'+i] + ' ' + data['meals'][0]['strIngredient'+i]+', ';
              //this.ingredients = data["meals"][0]["strIngredient"+i];
              this.ingredientsArray[i] = data['meals'][0]['strMeasure'+i] + ' ' + data['meals'][0]['strIngredient'+i];
              //console.log(this.ingredientsArray[i]);
          }
        }
        this.ingredientsArray= this.ingredientsArray.filter(Boolean);
        /* Vypis pole
        for(let i = 1; i <= 20; i++)
        {
            console.log(this.ingredientsArray[i]);
        }
        */
        let record = new HistoryRecord(this.enteredInput, this.foodName, this.instructionTitle, this.instructions, this.ingredientsTitle, this.ingredientsArray, this.foodImage);
        console.log(record);
        this.historyService.saveRecord(record);
        this.loadingDialog.dismiss();
        console.log(data);
      });
    }
    else{
      this.presentAlert();
    }
  }
  async presentLoading() 
  {
    this.loadingDialog = await this.loadingController.create(
    {
      message: 'Searching ...', 
    });
    await this.loadingDialog.present();
  }  

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Wrong input',
      message: 'The input is empty or incorrect. Check your input, please and try again.',
      buttons: ['OK']
    });

    await alert.present();
  }
}


