export class HistoryRecord 
{
    enteredInput: String;
    foodName: String;
    instructionTitle: String;
    instructions: String;
    ingredientTitle: String;
    ingredientsArray:Array<string> = new Array(); 
    foodImage: String
    constructor(enteredInput: String, foodName: String, instructionTitle: String, instructions: String, ingredientTitle: String, ingredientsArray: Array<string>, foodImage: String) 
    {
        this.enteredInput = enteredInput;
        this.foodName = foodName;
        this.instructionTitle = instructionTitle;
        this.instructions = instructions;
        this.ingredientTitle = ingredientTitle;
        this.ingredientsArray = ingredientsArray;
        this.foodImage = foodImage;
    }
}