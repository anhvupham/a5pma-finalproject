import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  
  public getMeal(text: String) 
  {
    https://www.themealdb.com/api/json/v1/1/search.php?s=bolognese
    return this.http.get('https://www.themealdb.com/api/json/v1/1/search.php?s='+text);
  }

  constructor(private http: HttpClient) 
  {
    
  }

}
